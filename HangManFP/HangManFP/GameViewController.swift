//
//  GameViewController.swift
//  HangManFP
//
//  Created by Norma Reyes on 10/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class GameViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate {

    let reuseIdentifier = "collectionCell"
    var charsFound:[Character] = Array(repeating: "_", count: 1)
    var inputChars:[String] = Array()
    var category:String = ""
    var wordManager:WordsManager?
    var palabra:String = ""
    var countError:Int = 0
    var word:Word?
    var score:Int = 0
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var collectionCharacters: UICollectionView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var inputsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        BackEndWordManager().selectWords{(result) in
            self.wordManager = WordsManager(words:result,category:self.category)
            self.initGame()
            
        }
        inputTextField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.palabra.characters.count)
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
 
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CharViewCell
        
        cell.charLabel.text = String(self.charsFound[indexPath.item])
        
        return cell
    }

    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        
    }*/

    
    @IBAction func backToLastState(_ sender: Any) {
        
        let alert = UIAlertController(title: "Stop to Play?", message: "Do you want to finish the game?", preferredStyle: .alert)
        let firstAction = UIAlertAction(title: "Yes", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                let alert = UIAlertController(title: "Your Score", message: String(self.score), preferredStyle: .alert)
                let firstAction = UIAlertAction(title: "OK", style: .default, handler : {action in
                    self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(firstAction)
                self.present(alert, animated: true, completion: nil)
        
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        })
        
        
        let cancelAction = UIAlertAction(title: "No", style: .default)
        
        alert.addAction(cancelAction)
        alert.addAction(firstAction)
        
        present(alert, animated: true, completion:nil)
        
        
    }

    /*
        This function is executed when there is a input char event
        it checks wheter the input char is contained for the word to find
     */
    @IBAction func editingChanged(_ sender: UITextField) {
 
        let inputChar = (sender.text!).uppercased()
        let abecedario = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
        
        if abecedario.contains(inputChar){
            if !inputChars.contains(inputChar){
                inputChars.append(inputChar)
                if(palabra.contains(inputChar)){
                    showCharacters(char:inputChar)
                }else{
                    manageError()
                }
                inputsLabel.text = inputChars.joined()
            }
            sender.text = ""
        }

            
        }
        
        
    /*
        It will print the corrects inputs caractes
        (in develop ...)
     */
    func showCharacters(char:String){
        
        let wordArray = Array(palabra.characters)
        let n:Int = wordArray.count - 1
        
        for index in 0...n {
            if(wordArray[index].description == char){
                charsFound[index] = wordArray[index]
            }
        }
        self.collectionCharacters.reloadData()
        checkWord()
    }
    
    /*
        Check if the input characters arrays is equal to the word
     */
    func checkWord(){
        let inputWord = String(Array(charsFound))
        
        if(palabra == inputWord){
            showMessage(title: "WELL DONE", message: "YOU WIN \n \(palabra)")
            score = score + 10
            scoreLabel.text = String(score)
        }
    }
    
    /*
        Check if the numer of errors is minor than the maximun number of errors allowed (7)
     */
    func manageError(){
        countError += 1
        if(countError < 7){
            image.image = UIImage(named: "elahorcado\(countError)")
        }else{
            
            showMessage(title: "GAME OVER", message: "YOU ARE A LOSER \n \(palabra)")
            
            score = score - 5
            scoreLabel.text = String(score)
        }
    }
    
    
    /*
        Initialice the enviroment of this litle game
        Initialice the variable for the game
     */
    func initGame(){
        
        countError = 0
        image.image = UIImage(named: "elahorcado\(countError)")
        
        /*
            searchWordById() is the function that retunr a new word for playing the game
            It petains to WordManager class
        */
        let word = self.wordManager?.searchWordById()
        self.palabra = (word?.palabra?.uppercased())!
        
        let numberOfCharacters = (self.palabra.characters.count)
        self.charsFound = Array(repeating: "-", count: numberOfCharacters)
        
        self.quitarEspaciosIntermedios(palabra:self.palabra)
        
        self.palabra = quitarTildados(palabra:self.palabra)
        
        self.inputChars = Array()
        self.inputsLabel.text = ""
        
        self.collectionCharacters.reloadData()
        
    }
    
    /*
        Show a popup whit a title and a body
        @parmas 
        title:String -> the title of the popup
        message:String -> the message bofy of the popup
     */
    
    func showMessage(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(firstAction)
        present(alert, animated: true, completion:{ action in
            self.initGame()
        })
        
        //initGame()
        
    }
    
    func quitarEspaciosIntermedios(palabra:String){
        
        let wordArray = Array(palabra.characters)
        let n:Int = wordArray.count - 1
        
        for index in 0...n {
            if(wordArray[index].description == " "){
                charsFound[index] = wordArray[index]
            }
        }
    }
    
    func quitarTildados(palabra:String) -> String{
        
  
        var palabraProcesada:String = palabra
        let tildados: [String: String] = ["Á":"A","É":"E","Í":"I","Ó":"O","Ú":"U"]
        
        for (tildado, llano) in tildados {
            if palabra.contains(tildado) {
                palabraProcesada = palabraProcesada.replacingOccurrences(of: tildado, with: llano)
            }
            
        }
        return palabraProcesada
    }
    
}
