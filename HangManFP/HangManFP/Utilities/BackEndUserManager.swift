//
//  BackEndUserManager.swift
//  HangManFP
//
//  Created by Norma Reyes on 12/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import Foundation


class BackEndUserManager{

    func insertUser(user:User, completionHandler: @escaping ()-> Void) {
    
        let urlString = "https://hangman-ios.mybluemix.net/user?\(user.toString())"
        
        Alamofire.request(urlString).responseJSON { response in
            print(response)
            DispatchQueue.main.async {
                completionHandler()
            }
        }
    
    }
    
    func findUser(user:User,completionHandler: @escaping (User)-> Void) {
        
        let urlString = "https://hangman-ios.mybluemix.net/findUser?\(user.toString())"
        
        Alamofire.request(urlString).responseObject {  (response: DataResponse<User>)  in
            let userResponse = response.result.value
            
            if let user = userResponse {
                DispatchQueue.main.async {
                    completionHandler(user)
                }
            }
        }
        
    }
    
}
