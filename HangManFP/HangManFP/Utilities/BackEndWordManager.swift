//
//  BackEndWordManager.swift
//  HangManFP
//
//  Created by Norma Reyes on 12/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import Foundation

class BackEndWordManager{
    
    func selectWords(completionHandler: @escaping ([Word])-> Void) {
        
        let urlString = "https://hangman-ios.mybluemix.net/findAll"
        
        Alamofire.request(urlString).responseObject {  (response: DataResponse<WordsArray>)  in
            let wordsResponse = response.result.value
            
            if let words = wordsResponse?.words {
                DispatchQueue.main.async {
                    completionHandler(words)
                }
            }
        }
        
    }
    
    
}
