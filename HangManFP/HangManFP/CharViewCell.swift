//
//  CharViewCell.swift
//  HangManFP
//
//  Created by Norma Reyes on 15/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class CharViewCell: UICollectionViewCell {
    
    @IBOutlet weak var charLabel: UILabel!
    
}
