//
//  GameModeViewController.swift
//  HangManFP
//
//  Created by Norma Reyes on 10/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class GameModeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func leaveGame(_ sender: Any) {
        self.dismiss(animated: true
            , completion: nil)
        
    }
    
    
    
}
