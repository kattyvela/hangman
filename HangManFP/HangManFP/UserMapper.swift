//
//  UserMapper.swift
//  HangManFP
//
//  Created by Norma Reyes on 13/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import ObjectMapper

class User:Mappable {
    
    var id:String?
    var username:String?
    var password:String?
    
    
    required init() {
        
    }
    
    required init(map:Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        password <- map["password"]
    }
    
    func toString() -> String{
        return "username=\(self.username!), password:\(self.password!)}"
    }
    
}
