//
//  WordMapper.swift
//  HangManFP
//
//  Created by Norma Reyes on 13/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import ObjectMapper

class WordsArray:Mappable{
    
    var words:[Word]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        words <- map["result"]
    }
}


class Word:Mappable {
    
    var id:String?
    var categoria:String?
    var palabra:String?
    
    required init() {
    }
    
    required init(map:Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        categoria <- map["categoria"]
        palabra <- map["palabra"]
    }
    
    func toString() -> String{
        return "{id: \(self.id!), palabra:\(self.palabra!), categoria:\(self.categoria!)}"
    }
    
}



