//
//  ChooseGameViewController.swift
//  HangManFP
//
//  Created by Norma Reyes on 10/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ChooseGameViewController: UIViewController {

    var category:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func backToGameMode(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    
    }
    
    func goToGame(){
        performSegue(withIdentifier: "toGameSegue",
                     sender: self)
    }
    
    
    @IBAction func celecritiesAction(_ sender: Any) {
        category = "F"
        goToGame()
    }
   

    
    @IBAction func countriesAction(_ sender: Any) {
        category = "P"
        goToGame()
    }
    
    
    @IBAction func sportAction(_ sender: Any) {
        category = "D"
        goToGame()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toGameSegue" {
            let destination = segue.destination as! GameViewController
            destination.category = self.category;
        }
    }

}
