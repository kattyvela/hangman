//
//  WordsManager.swift
//  HangManFP
//
//  Created by Norma Reyes on 13/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation

class WordsManager {
    
    var  words:[Word]?
    
    required init(words:[Word],category:String) {
        
        if category == "" {
            self.words = words
        }
        else{
            self.words = self.getCategoryWords(words:words,category: category)
        }
        
    }
    
    /*
        retunr a word of the wordsArray
     */
    func searchWordById() -> Word{
        return (words?[createRandom()])!
    }
    

    /*
        Create a Ramdom Int
    */
    private func createRandom() -> Int{
        let randomNum:UInt32 = arc4random_uniform(UInt32(words!.count))
        return Int(randomNum)
    }
    
    private func getCategoryWords(words:[Word],category:String)->[Word]{
        
        var categoryWords:[Word] = []
        for word in words {
            if word.categoria == category {
                categoryWords += [word]
            }
        }
        
        return categoryWords
    }
    
    
}
